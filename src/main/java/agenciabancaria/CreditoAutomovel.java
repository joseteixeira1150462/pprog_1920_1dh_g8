package agenciabancaria;

/**
 *
 * @author 1150462 & 1192223
 */
public class CreditoAutomovel extends CreditoSemCarencia {
   
    /**
     * Atributo de classe da percentagem do desconto a aplicar em emprestimos a curto prazo.
     */
    private static double desconto = 0.01;
    
    /**
     * Atributo de classe, é o prazo de empréstimo máximo para um crédito ser considerado como curto.
     */
    private static int limiteParaDesconto = 24;
    
    /**
     * Atributo de classe da percentagem do juro anual.
     */
    private static double juroAnual = 0.06;

    /**
     * Contador de crédito ao consumo, adicionado por composição, que permite contar o número de créditos ao consumo existentes.
     */
    private ContadorCreditoConsumo CCC = new ContadorCreditoConsumo();

    /**
     * Construtor de credito automovel por omissão.
     */
    public CreditoAutomovel() {
        super();
    }

    /**
     *  Construtor compelto do credito automovel com os seguntes parâmetros:
     * @param nomeCliente
     * @param profissao
     * @param montante
     * @param prazoFinanciamento
     */
    public CreditoAutomovel( String nomeCliente, String profissao, double montante, int prazoFinanciamento) {
        super(nomeCliente, profissao, montante, prazoFinanciamento);
    }

    /**
     * Método que permite calcular o montante a receber por cada crédito automóvel.
     * @return calcularMontanteAReceberPorCadaCredito()
     */
    @Override
    public double calcularMontanteAReceberPorCadaCredito() {
        if(getPrazoFinanciamento() <= limiteParaDesconto) {
            return calcularMontanteTotalJuros() + (getMontante() * (1.0 - desconto));
        } else {
            return super.calcularMontanteAReceberPorCadaCredito();
        }
    }

    /**
     * Método que permite calcular o montante total de juros a pagar.
     * @return calcularMontanteTotalJuros()
     */
    @Override 
    public double calcularMontanteTotalJuros() {
        if(getPrazoFinanciamento() <= limiteParaDesconto) {
            return super.calcularMontanteTotalJuros() * (1.0 - desconto);
        } else {
            return super.calcularMontanteTotalJuros();
        }
    }

    /**
     * Método que apresenta uma descrição detalhada do objeto.
     */
    @Override 
    public String toString() {
        return super.toString() + "\n"
                + "Desconto: " + ((getPrazoFinanciamento() <= limiteParaDesconto)? (CreditoAutomovel.desconto * 100) : 0 ) + "%\n" ;
    }

    /**
     * Método que retorna o valor do atributo desconto.
     * @return desconto
     */
    public static double getDesconto() {
        return desconto;
    }

    /**
     * Método que permite alterar o valor do atributo de classe do desconto
     * @param desconto
     */
    public static void setDesconto(double desconto) {
        CreditoAutomovel.desconto = desconto;
    }

    /**
     * Método que retorna o valor do atributo limite do desconto
     * @return
     */
    public static int getLimiteParaDesconto() {
        return limiteParaDesconto;
    }

    /**
     * Método que permite alterar o valor do atributo limite do desconto
     * @param limiteParaDesconto
     */
    public static void setLimiteParaDesconto(int limiteParaDesconto) {
        CreditoAutomovel.limiteParaDesconto = limiteParaDesconto;
    }

    /**
     * Método que retorna o valor do atributo juro anual
     * @return
     */
    @Override 
    public double getJuroAnual() {
        return juroAnual;
    }

    /**
     * Método que permite alteral o valor do atributo de classe do juro anual
     * @param juroAnual
     */
    @Override 
    public void setJuroAnual(double juroAnual) {
        CreditoAutomovel.juroAnual = juroAnual;
    }
}
