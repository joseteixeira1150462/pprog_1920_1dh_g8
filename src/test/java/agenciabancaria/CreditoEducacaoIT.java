package agenciabancaria;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Zeca Teixeira
 */
public class CreditoEducacaoIT {

    public CreditoEducacaoIT() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of calcularMontanteAReceberPorCadaCredito method, of class
     * CreditoEducacao.
     */
    @Test
    public void testCalcularMontanteAReceberPorCadaCredito() {
        System.out.println("calcularMontanteAReceberPorCadaCredito");
        CreditoEducacao instance = new CreditoEducacao("Catarina", "Unemployed", 18000, 60, 24);
        double expResult = 19275.0;
        double result = instance.calcularMontanteAReceberPorCadaCredito();

        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of calcularMontanteTotalJuros method, of class CreditoEducacao.
     */
    @Test
    public void testCalcularMontanteTotalJuros() {
        System.out.println("calcularMontanteTotalJuros");
        CreditoEducacao instance = new CreditoEducacao("Catarina", "Unemployed", 18000, 60, 24);
        double expResult = 1275.0;
        double result = instance.calcularMontanteTotalJuros();
        assertEquals(expResult, result, 0.0);
    }

}
