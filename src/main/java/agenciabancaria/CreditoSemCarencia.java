package agenciabancaria;

/**
 *
 * @author Zeca Teixeira
 */
public abstract class CreditoSemCarencia extends CreditoBancario {

    /**
     * Método que permite calcular o montante total com juros a pagar no crédito
     * sem carência pelo cliente.
     *
     * @return Juros (em euros)
     */
    @Override
    public double calcularMontanteTotalJuros() {
        double creditoAmortizarMensalmente = getMontante() / getPrazoFinanciamento();
        double juroAnual = getJuroAnual();
        final double juroMensal = getJuroAnual() / 12;
        double juros = 0;
        double montanteEmDivida = getMontante();
        for (int mes = 1; mes <= getPrazoFinanciamento(); mes++) {
            juros += montanteEmDivida * juroMensal;
            montanteEmDivida -= creditoAmortizarMensalmente;
        }
        return juros;
    }

    /**
     * Construtor de crédito sem carência por omissão
     */
    public CreditoSemCarencia() {
        super();
    }

    /**
     * Construtor completo do crédito sem carência com os seguintes parâmetros:
     * @param nomeCliente
     * @param profissao
     * @param montante
     * @param prazoFinanciamento
     */
    public CreditoSemCarencia(String nomeCliente, String profissao, double montante, int prazoFinanciamento) {
        super(nomeCliente, profissao, montante, prazoFinanciamento);
    }
}
