package agenciabancaria;

/**
 *
 * @author 1150462 & 1192223
 */
public class CreditoHabitacao extends CreditoSemCarencia {

    /**
     * Atributo de instância do spread do crédito do cliente
     */
    private double spread;

    /**
     * Atributo de classe da taxa euribor pré-fixa do crédito do cliente
     */
    private static double euribor = 0.001;

    /**
     * Atributo de classe do número de créditos à habitação
     */
    private static int n_creditosHabitacao = 0;

    /**
     * Atributo de classe do spread do crédito do cliente
     */
    private static final double SPREAD_POR_OMISSAO = 0;

    /**
     * Construtor de crédito de habitação por omissão
     */
    public CreditoHabitacao() {
        super();
        setSpread(SPREAD_POR_OMISSAO);
    }

    /**
     * Construtor completo do crédito habitação com os seguintes parâmetros:
     *
     * @param nomeCliente
     * @param profissao
     * @param montante
     * @param prazoFinanciamento
     * @param spread
     */
    public CreditoHabitacao(String nomeCliente, String profissao, double montante, int prazoFinanciamento, double spread) {
        super(nomeCliente, profissao, montante, prazoFinanciamento);
        setSpread(spread);
        n_creditosHabitacao++;
    }

    /**
     * Método que apresenta uma descrição detalhada do objeto
     */
    @Override
    public String toString() {
        return super.toString() + "\n"
                + "Spread: " + this.spread;
    }

    /**
     * Método que retorna o conteúdo do spread do crédito do cliente
     *
     * @return spread
     */
    public double getSpread() {
        return spread;
    }

    /**
     * Método que retorna o conteúdo do atributo da taxa euribor do crédito do
     * cliente.
     *
     * @return A euribor a 12 meses.
     */
    public static double getEuribor() {
        return euribor;
    }

    /**
     * Método (implementado) que retorna o conteúdo do atributo do juro anual a
     * pagar pelo cliente
     *
     * É necessário recalcular sempre um novo juro anual visto que nunca sabemos
     * quando a euribor muda
     *
     * @return euribor + spread
     */
    @Override
    public double getJuroAnual() {

        return euribor + spread;
    }

    /**
     * Método que permite alterar o conteúdo do atributo do spread do crédito do
     * cliente
     *
     * @param euribor A auribor a 12 meses.
     */
    public static void setEuribor(double euribor) {
        CreditoHabitacao.euribor = euribor;
    }

    /**
     * Método que permite alterar o conteúdo do atributo da taxa euribor do
     * crédito do cliente
     *
     * @param spread
     */
    public void setSpread(double spread) {
        this.spread = spread;
    }

    /**
     * Método que permite alterar o conteúdo do atributo do juro anual a pagar
     * pelo cliente
     *
     * Para ajustar o juro anual, visto que a taxa euribor é fixa, é
     * necessário calcular um novo spread juroAnual = euribor + spread <=>
     * juroAnual - euribor = spread
     *
     * @param juroAnual
     */
    @Override
    public void setJuroAnual(double juroAnual) {
        this.spread = juroAnual - euribor;
    }

    /**
     * Método que retorna o conteúdo do número de créditos à habitação
     * @return n_creditosHabitacao
     */
    public static int get_n_creditosHabitacao() {
        return n_creditosHabitacao;
    }
}
