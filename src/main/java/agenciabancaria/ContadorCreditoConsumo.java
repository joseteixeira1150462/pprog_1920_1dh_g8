package agenciabancaria;

public class ContadorCreditoConsumo {

    /**
     * Atributo de classe do número de créditos de consumo do cliente existentes.
     */
    private static int nCreditosConsumo;

    /**
     * Construtor de contador de créditos por consumo do cliente
     * OBS: incrementa o número de créditos por consumo por cliente
     */
    ContadorCreditoConsumo() {
        nCreditosConsumo++;
    }

    /**
     * Método que retorna o conteúdo do número de créditos por consumo
     */
    public static int getnCreditosConsumo() {
        return nCreditosConsumo;
    }
}
