/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenciabancaria;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author Zeca Teixeira
 */
public class CreditoAutomovelIT {

    public CreditoAutomovelIT() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of calcularMontanteAReceberPorCadaCredito method, of class
     * CreditoAutomovel.
     */
    @Test
    public void testCalcularMontanteAReceberPorCadaCredito() {
        System.out.println("calcularMontanteAReceberPorCadaCredito");
        CreditoAutomovel instance = new CreditoAutomovel("Inês", "Teacher", 60000, 24);
        double expResult = 63112.5;
        double result = instance.calcularMontanteAReceberPorCadaCredito();

        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of calcularMontanteTotalJuros method, of class CreditoAutomovel.
     */
    @Test
    public void testCalcularMontanteTotalJuros() {
        System.out.println("calcularMontanteTotalJuros");
        CreditoAutomovel instance = new CreditoAutomovel("Inês", "Teacher", 60000, 24);
        double expResult = 3712.5;
        double result = instance.calcularMontanteTotalJuros();

        assertEquals(expResult, result, 0.0);

    }


}
