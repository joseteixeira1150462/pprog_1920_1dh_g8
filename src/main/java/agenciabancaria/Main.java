package agenciabancaria;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 1150462 & 1192223
 */
public class Main {

    /**
     * Ponto de entrada do programa
     * @param args
     */
    public static void main(String[] args) {
        // A)
        CreditoHabitacao h1 = new CreditoHabitacao("Connor", "Entertainer", 120000, 240, 0.01);
        CreditoHabitacao h2 = new CreditoHabitacao("Diogo", "Student", 100000, 240, 0.02);
        CreditoAutomovel a1 = new CreditoAutomovel("Tiago", "Student", 60000, 25);
        CreditoAutomovel a2 = new CreditoAutomovel("Inês", "Teacher", 60000, 24);
        CreditoEducacao e1 = new CreditoEducacao("Catarina", "Unemployed", 18000, 60, 24);
        CreditoEducacao e2 = new CreditoEducacao("Mónica", "Artist", 20000, 60, 24);

        // B)
        List<CreditoBancario> lista = new ArrayList<>();
        lista.add(h1);
        lista.add(h2);
        lista.add(a1);
        lista.add(a2);
        lista.add(e1);
        lista.add(e2);

        // C)
        // 1)
        System.out.println("Listagem C) 1) \"Nome do cliente e valor a receber até ao final de cada crédito ao consumo\"");
        for (CreditoBancario credito : lista) {
            if (!(credito instanceof CreditoHabitacao)) {
                System.out.println("\tNome: " + credito.getNomeCliente()
                        + "\n\tMontante a receber: " + credito.calcularMontanteAReceberPorCadaCredito()
                        + "\n\t   ***   ***   ***   ***   ***");
            }
        }
        System.out.print("\n");
        // 2)
        System.out.println("Listagem C) 2) \"Nome do cliente e valor dos juros (para além do montante do empréstimo) de cada contrato de crédito bancário\"");
        for (CreditoBancario credito : lista) {
            System.out.println("\tNome: " + credito.getNomeCliente()
                    + "\n\tMontante a receber (juros): " + credito.calcularMontanteTotalJuros()
                    + "\n\tMontante emprestado: " + credito.getMontante()
                    + "\n\t   ***   ***   ***   ***   ***");
        }
        System.out.println("\n");

        // D)
        System.out.println("Listagem D)\n\tNúmero de créditos à habitação: " + CreditoHabitacao.get_n_creditosHabitacao()
                + "\n\tNúmero de créditos ao consumo: " + ContadorCreditoConsumo.getnCreditosConsumo() + "\n");

        // E)
        float valor_total = 0;
        float valor_juros = 0;
        for (CreditoBancario credito : lista) {
            valor_total = (float) credito.calcularMontanteAReceberPorCadaCredito();
            valor_juros = (float) credito.calcularMontanteTotalJuros();
        }
        System.out.println("LISTAGEM E)\n\tValor total: " + valor_total + "€\n\tValor em juros: " + valor_juros + "€\n");
    }

}
