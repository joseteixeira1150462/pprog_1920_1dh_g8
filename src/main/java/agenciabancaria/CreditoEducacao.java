package agenciabancaria;

/**
 *
 * @author 1150462 & 1192223
 */
public class CreditoEducacao extends CreditoBancario {

    /**
     * Atributo de classe do juro anual pré-fixo a pagar pelo cliente.
     */
    private static double juroAnual = 0.02;

    /**
     * Atributo de instância do período de carência do cliente.
     */
    private int periodoCarencia;

    /**
     * Contador de crédito ao consumo, adicionado por composição, que permite contar o número de créditos ao consumo existentes.
     */
    private ContadorCreditoConsumo CCC = new ContadorCreditoConsumo();

    /**
     * Construtor do crédito de educação por omissão.
     */
    public CreditoEducacao() {
        super();
        this.periodoCarencia = periodoCarencia;
    }

    /**
     * Construtor completo do crédito educação com os seguintes parâmetros:
     *
     * @param nomeCliente
     * @param profissao
     * @param montante
     * @param prazoFinanciamento
     * @param periodoCarencia
     */
    public CreditoEducacao(String nomeCliente, String profissao, double montante, int prazoFinanciamento, int periodoCarencia) {
        super(nomeCliente, profissao, montante, prazoFinanciamento);
        this.periodoCarencia = periodoCarencia;
    }

    /**
     * Método (implementado) que permite calcular o montante total com juros a
     * pagar pelo cliente
     *
     * @return juros
     */
    @Override
    public double calcularMontanteTotalJuros() {
        double creditoAmortizarMensalmente = getMontante() / (getPrazoFinanciamento() - getPeriodoCarencia());
        final double juroMensal = getJuroAnual() / 12;

        double juros = 0;
        double montanteEmDivida = getMontante();
        for (int mes = 1; mes <= getPeriodoCarencia(); mes++) {
            juros += montanteEmDivida * juroMensal;
        }
        for (int mes = getPeriodoCarencia() + 1; mes <= getPrazoFinanciamento(); mes++) {
            juros += montanteEmDivida * juroMensal;
            montanteEmDivida -= creditoAmortizarMensalmente;
        }
        return juros;
    }

    /**
     * Método que apresenta uma descrição detalhada do objeto
     */
    @Override
    public String toString() {
        return super.toString() + "\n"
                + "Periodo de Carencia: " + this.getPeriodoCarencia() + "meses\n";
    }

    /**
     * Método que retorna o conteúdo do atributo do período de carência do
     * cliente
     *
     * @return periodoCarencia
     */
    public int getPeriodoCarencia() {
        return periodoCarencia;
    }

    /**
     * Método (implementado) que retorna o conteúdo do atributo de classe do
     * juro anual a pagar pelo cliente
     *
     * @return juroAnual
     */
    @Override
    public double getJuroAnual() {
        return CreditoEducacao.juroAnual;
    }

    /**
     * Método que permite alterar o conteúdo do atributo do período de carência
     * do cliente
     *
     * @param periodoCarencia
     */
    public void setPeriodoCarencia(int periodoCarencia) {
        this.periodoCarencia = periodoCarencia;
    }

    /**
     * Método que permite alterar o conteúdo do atributo de classe do juro anual
     * a pagar pelo cliente
     *
     * @param juroAnual
     */
    @Override
    public void setJuroAnual(double juroAnual) {
        CreditoEducacao.juroAnual = juroAnual;
    }
}
