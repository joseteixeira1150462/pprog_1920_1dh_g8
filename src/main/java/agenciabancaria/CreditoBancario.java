package agenciabancaria;

/**
 *
 * @author 1150462 & 1192223
 */
public abstract class CreditoBancario {

    /**
     * Atributo de instância do nome do cliente
     */
    private String nomeCliente;
    /**
     * Atributo de instância da profissão do cliente
     */
    private String profissao;
    /**
     * Atributo de instância do montante a ser financiado ao cliente.
     */
    private double montante;
    /**
     * Atributo de instância do prazo de financiamento do cliente
     */
    private int prazoFinanciamento;

    /**
     * Atributo de classe do número de créditos do cliente
     */
    private static int n_creditos = 0;

    
    /**
     * Nome do cliente por omissão
     */
    private static final String NOME_CLIENTE_POR_OMISSAO = "DESCONHECIDO";
    /**
     * Profissão do cliente por omissão
     */
    private static final String PROFISSAO_POR_OMISSAO = "DESCONHECIDO";
    /**
     * Montante a pagar pelo cliente por omissão
     */
    private static final double MONTANTE_POR_OMISSAO = 0;
    /**
     * Prazo de financiamento (meses) do cliente por omissão
     */
    private static final int FINANCIAMENTO_POR_OMISSAO = 0;

    /**
     * Construtor do crédito bancário por omissão de parámetros.
     */
    public CreditoBancario() {
        this.nomeCliente        = NOME_CLIENTE_POR_OMISSAO;
        this.profissao          = PROFISSAO_POR_OMISSAO;
        this.montante           = MONTANTE_POR_OMISSAO;
        this.prazoFinanciamento = FINANCIAMENTO_POR_OMISSAO;
    }

    /**
     * Construtor completo do crédito bancário.
     * @param nomeCliente Nome do cliente para quem se destina o crédito.
     * @param profissao Profissão do cliente.
     * @param montante Montante a ser financiado pelo banco.
     * @param prazoFinanciamento Parazo (em meses) em que o cliente terá que liquidar a sua dívida.
     */
    public CreditoBancario( String nomeCliente, String profissao, double montante, int prazoFinanciamento) {
        this.nomeCliente        = nomeCliente        ;
        this.profissao          = profissao          ;
        this.montante           = montante           ;
        this.prazoFinanciamento = prazoFinanciamento ;
    }

    /**
     * Método que permite calcular o montante a receber por cada crédito do cliente.
     * @return O valor dos juros em conjunto com o valor inicial.
     */
    public double calcularMontanteAReceberPorCadaCredito() { return calcularMontanteTotalJuros() + getMontante(); }

    /**
     * Método abstrato para o cálculo do montante total com juros a pagar pelo cliente.
     * @return O valor total que o cliente terá que pagar em juros.
     */
    public abstract double calcularMontanteTotalJuros();

    /**
     * Método que retorna o conteúdo do atributo do nome do cliente.
     * @return nomeCliente
     */
    public String getNomeCliente() {
        return nomeCliente;
    }

    /**
     * Método que retorna o conteúdo do atributo da profissão do cliente.
     * @return profissao
     */
    public String getProfissao() {
        return profissao;
    }

    /**
     * Método que retorna o valor do atributo do montante emprestado ao cliente.
     * @return Montante (em euros) financiado pelo banco.
     */
    public double getMontante() {
        return montante;
    }

    /**
     * Método que retorna o valor do atributo do prazo de financiamento (meses) do cliente.
     * @return prazoFinanciamento
     */
    public int getPrazoFinanciamento() {
        return prazoFinanciamento;
    }

    /**
     * Método abstrato que retorna o juro anual a pagar pelo cliente.
     * @return A taxa de juro anual do crédito.
     */
    public abstract double getJuroAnual();

    /**
     * Método que permite alterar o conteúdo do atributo nome do cliente.
     * @param nomeCliente O nome do cliente.
     */
    public void setNomeCliente (String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    /**
     * Método que permite alterar o conteúdo do atributo profissão do cliente.
     * @param profissao A profissão do cliente.
     */
    public void setProfissao (String profissao) {
        this.profissao = profissao;
    }

    /**
     * Método que permite alterar o conteúdo do atributo montante.
     * @param montante
     */
    public void setMontante (double montante) {
        this.montante = montante;
    }

    /**
     * Método que permite alterar o conteúdo do atributo do prazo de financiamento do cliente.
     * @param prazoFinanciamento (em meses)
     */
    public void setPrazoFinanciamento (int prazoFinanciamento) {
        this.prazoFinanciamento = prazoFinanciamento;
    }

    /**
     * Método abstrato que permite alterar o juro anual a pagar pelo cliente.
     * @param juroAnual
     */
    public abstract void setJuroAnual ( double juroAnual );

    /**
     * Método que apresenta uma descrição detalhada do objeto.
     */
    @Override
    public String toString() {
        return    "Nome: " + this.nomeCliente + "\n"
                + "Profissão: " + this.profissao + "\n"
                + "Montante: " + this.montante + "€\n"
                + "Prazo Financiamento: " + this.prazoFinanciamento
                + "Juro Anual: " + (getJuroAnual() * 100) + "%\n";
    }


    /**
     * Método que retorna o valor do número de créditos bancários de cada cliente
     * @return n_creditos
     */
    public static int getN_creditos() {
        return n_creditos;
    }

}
