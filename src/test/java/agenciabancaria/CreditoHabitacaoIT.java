/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenciabancaria;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 *
 * @author Zeca Teixeira
 */
public class CreditoHabitacaoIT {

    public CreditoHabitacaoIT() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of calcularMontanteTotalJuros method, of class CreditoHabitacao
     * implemented on CreditoSemCarencia.
     */
    @Test
    public void testCalcularMontanteTotalJuros() {
        System.out.println("calcularMontanteTotalJuros");
        CreditoHabitacao instance = new CreditoHabitacao("Connor", "Entertainer", 120000, 240, 0.01);
        double expResult = 13255.0;
        double result = instance.calcularMontanteTotalJuros();

        assertEquals(expResult, result, 0.0);

    }

    /**
     * Test of calcularMontanteTotalJuros method, of class CreditoHabitacao
     * implemented on CreditoBancario
     */
    @Test
    public void testCalcularMontanteAReceberPorCadaCredito() {
        System.out.println("calcularMontanteAReceberPorCadaCredito");
        CreditoHabitacao instance = new CreditoHabitacao("Connor", "Entertainer", 120000, 240, 0.01);
        double expResult = 133255;
        double result = instance.calcularMontanteAReceberPorCadaCredito();

        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getJuroAnual method, of class CreditoHabitacao.
     */
    @Test
    public void testGetJuroAnual() {
        System.out.println("getJuroAnual");
        CreditoHabitacao instance = new CreditoHabitacao("Connor", "Entertainer", 120000, 240, 0.01);
        double expResult = 0.011; //spread + euribor
        double result = instance.getJuroAnual();
        assertEquals(expResult, result, 0.0);
    }

}
